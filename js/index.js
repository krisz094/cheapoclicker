//html elemek bekötése
var creditBtn = document.getElementById("hit")
var creditCount = document.getElementById("creditCount")
var perSecCount = document.getElementById("perSecCount")
var perClickCount = document.getElementById("perClickCount")
var upgradesDiv = document.getElementById("upgrades")
var buildingsDiv = document.getElementById("buildings")
var unlockedUpgrades = document.getElementById("unlockedUpgrades")


/**
 * épület osztálydefiníció
 */
class Building {
	/**
	 * épület osztály
	 * @param {number} id
	 * @param {string} name 
	 * @param {number} basePrice alap ár
	 * @param {number} baseCreditsPerSec kreditek/rang 
	 * @param {Upgrade[]} relUpgrades az épület számára releváns fejlesztések
	 */
	constructor(id, name, basePrice, baseCreditsPerSec, relUpgrades) {
		this.id = id
		this.name = name
		this.basePrice = basePrice
		this.baseCreditsPerSec = baseCreditsPerSec
		this.relUpgrades = relUpgrades
	}
	/**
	 * visszaadja h 1 secben hány pénzt ad az épület
	 * @param {*} plus ha X ranggal későbbi állapotot akarunk vizsgálni, meg kell adni
	 */
	creditsPerSec(plus = 0) {
		//megszorozza a ranggal a krediteket, 1 1 tizedesjegyre kerekít
		let cps = Math.round(this.baseCreditsPerSec * (this.getRank() + plus) * 10) / 10
		//megnézi a releváns fejlesztéseket, az "effekt"jüket kifejti a cookie/sec-re
		for (let currU of this.relUpgrades) {
			if (state.upgradesUnlocked[currU]) {
				cps = upgrades[currU].effect(cps)
			}
		}
		return cps
	}
	getRank() {
		return state.buildingRanks[this.id]
	}
	/**
	 * ár: bázis ár * 1.15 ^ rang
	 */
	price() {
		return Math.floor(this.basePrice * Math.pow(1.15, this.getRank()))
	}
	buyAble() {
		return this.price() <= state.credits
	}
}


class Upgrade {
	/**
	 * fejlesztés osztály
	 * @param {nuimber} id
	 * @param {string} name 
	 * @param {number} price 
	 * @param {function} condition feltétel, hogy meg lehessen venni
	 * @param {function} effect a fejlesztés hatása
	 */
	constructor(id, name, price, condition, effect) {
		this.id = id
		this.condition = condition
		this.name = name
		this.price = price
		this.effect = effect
	}
	buyAble() {
		return this.price <= state.credits
	}
}
/**
 * @type {Array.<Building>}
 */
const buildings = [
	new Building(0, "auto work", 10, 0.1, [0, 1, 2, 3]),
	new Building(1, "hamburger stand", 100, 1, [4, 5, 6, 7]),
	new Building(2, "gardening", 1100, 8, [8, 9, 10, 11]),
	new Building(3, "dik", 12000, 47, []),
	new Building(4, "hhe", 130000, 260, []),
	new Building(5, "asdddfd", 1400000, 1400, []),
	new Building(6, "asdddfdffff", 20000000, 7800, []),
	new Building(7, "asdddhhhh", 330000000, 44000, [])
]
//ezeket a függvényeket kaphatják meg a fejlesztések

//aki ezt kapja meg, az duplázza azokat, akikre releváns
var doubler = input => input * 2

//akik ezt kapják meg, azok annyival növelik meg a releváns dolgot, ahanyas szintű az első épület, megszorozva 10-zel. 
var elsoEpuletRanggalNovel = input => input + state.buildingRanks[0] * 10

//akik ezt kapják meg azok az auto work-ön kívüli épületek rangjának szummája * 0.1 pénzt kapnak /sec
var klikkenKivuliRanggalNovel = input => {
	var final = input
	for (let i = 1; i < buildings.length; i++) {
		final += Math.round(buildings[i].getRank() * 0.1 * 10) / 10
	}
	return final
}
var plus1percent = input => input * 1.01


/**
 * @type {Array.<Upgrade>}
 */
const upgrades = [
	//autoclick+sima click upgrade-jei
	new Upgrade(0, "work*2", 100, () => {
		return state.credits > 50 && state.buildingRanks[0] >= 1
	}, doubler),
	new Upgrade(1, "work*2", 500, () => {
		return state.credits > 100 && state.buildingRanks[0] >= 1
	}, doubler),
	new Upgrade(2, "work*2", 10000, () => {
		return state.credits > 5000 && state.buildingRanks[0] >= 20
	}, doubler),
	new Upgrade(3, "work+masrang", 100000, () => {
		return state.credits > 50000 && state.buildingRanks[0] >= 40
	}, klikkenKivuliRanggalNovel),
	//hamburgeres upgrade-jei
	new Upgrade(4, "hambi*2", 1000, () => {
		return state.credits > 400 && state.buildingRanks[1] >= 1
	}, doubler),
	new Upgrade(5, "hambi*2", 5000, () => {
		return state.credits > 2000 && state.buildingRanks[1] >= 5
	}, doubler),
	new Upgrade(6, "hambi*2", 50000, () => {
		return state.credits > 20000 && state.buildingRanks[1] >= 25
	}, doubler),
	new Upgrade(7, "hambi*2", 5000000, () => {
		return state.credits > 1000000 && state.buildingRanks[1] >= 50
	}, doubler),
	//kertészkedés upgrade-jei
	new Upgrade(8, "garden*2", 11000, () => {
		return state.credits > 4000 && state.buildingRanks[2] >= 1
	}, doubler),
	new Upgrade(9, "garden*2", 55000, () => {
		return state.credits > 10000 && state.buildingRanks[2] >= 5
	}, doubler),
	new Upgrade(10, "garden*2", 550000, () => {
		return state.credits > 200000 && state.buildingRanks[2] >= 25
	}, doubler),
	new Upgrade(11, "garden*2", 55000000, () => {
		return state.credits > 20000000 && state.buildingRanks[2] >= 50
	}, doubler),
	//össz bevételre vonatkozik
	new Upgrade(12, "osszbev*1.01", 1000000, () => {
		return state.credits >= 1000000
	}, plus1percent),
	new Upgrade(13, "osszbev*1.01", 5000000, () => {
		return state.credits >= 5000000
	}, plus1percent),
	new Upgrade(14, "osszbev*1.01", 10000000, () => {
		return state.credits >= 10000000
	}, plus1percent),
	new Upgrade(15, "osszbev*1.01", 50000000, () => {
		return state.credits >= 50000000
	}, plus1percent)
]
/**
 * a klikkekre vonatkozó fejlesztések
 */
const clickRelUpgrades = [0, 1, 2, 3]
/**
 * a játék státusza
 */
var state = {
	credits: 0,
	buildingRanks: [0, 0, 0, 0, 0, 0, 0, 0],
	upgradesUnlocked: [0]
}
/**
 * játék állapot mentés
 */
function save() {
	localStorage.setItem("state", JSON.stringify(state))
}

const allPerSecRelUpgrades = [12, 13, 14, 15]
/**
 * kiszámolja, hogy összesen hány "süti" jön secenként
 */
function perSec() {
	let perSec = 0
	for (let building of buildings)
		perSec += building.creditsPerSec()

	for (let upgradeId of allPerSecRelUpgrades) {
		if (state.upgradesUnlocked[upgradeId]) {
			perSec = upgrades[upgradeId].effect(perSec)
		}
	}
	return perSec
}

/**
 * kiszámolja, hogy hány "süti" jár 1 klikért
 */
function creditsPerClick() {
	let click = 1

	for (let upgradeId of clickRelUpgrades) {
		if (state.upgradesUnlocked[upgradeId]) {
			click = upgrades[upgradeId].effect(click)
		}
	}

	return click
}
/**
 * 1 épületet megvesz
 * @param {Building} building 
 */
function buy(building) {
	if (building.buyAble()) {
		state.credits -= building.price()
		state.buildingRanks[building.id]++
		save()
	}
}
/**
 * 1 fejlesztést megvesz
 * @param {Upgrade} upgrade 
 */
function buyU(upgrade) {
	if (upgrade.buyAble()) {
		state.upgradesUnlocked[upgrade.id] = 1
	}
}
/**
 * létrehoz egy épületből 1 HTML elemet
 * @param {Building} building 
 */
function createBuildingElem(building) {
	let elem = document.createElement("div")

	let rankElem = document.createElement("div")
	let nameElem = document.createElement("div")
	let priceElem = document.createElement("div")
	let cPSElem = document.createElement("div")

	/*let margin = 20 + Math.trunc(building.getRank().toString().length) * 25 + "px"*/

	elem.style.position = "relative"
	rankElem.style.fontSize = "50px"

	/*rankElem.style.cssFloat = "right"*/

	rankElem.style.position = "absolute"
	rankElem.style.right = "5px"

	elem.style.color = "white"
	elem.style.backgroundColor = "rgba(255,255,255,0.1)"
	priceElem.style.color = building.buyAble() ? "#00ff00" : "red"

	rankElem.innerHTML = `${building.getRank()}`
	nameElem.innerHTML = `${building.name}`
	priceElem.innerHTML = `${building.price()}`
	cPSElem.innerHTML = `Money/sec at next: ${building.creditsPerSec(1)} (${building.baseCreditsPerSec}/rank)`

	elem.innerHTML = ''
	elem.appendChild(rankElem)
	elem.appendChild(nameElem)
	elem.appendChild(priceElem)
	elem.appendChild(cPSElem)


	elem.addEventListener("mouseup", () => {
		buy(building)
	})
	return elem
}
/**
 * létrehoz 1 fejlesztésből 1 HTML elemet
 * @param {Upgrade} upgrade 
 */
function createUpgradeElem(upgrade, noprice = false) {
	let elem = document.createElement("div")
	elem.setAttribute("id", "upgrade")
	let nameElem = document.createElement("div");
	let priceElem = document.createElement("div");

	nameElem.innerHTML = upgrade.name
	priceElem.innerHTML = upgrade.price

	nameElem.style.fontSize = "0.8em"
	priceElem.style.color = upgrade.buyAble() ? "#00ff00" : "red"

	elem.innerHTML = ''
	elem.appendChild(nameElem)
	!noprice && elem.appendChild(priceElem)

	!noprice && elem.addEventListener("mouseup", () => {
		buyU(upgrade)
	})

	return elem
}

/**
 * egy képkocka renderelése
 */
function renderState() {
	//render left side
	creditCount.innerHTML = `${Math.floor(state.credits)}`
	perSecCount.innerHTML = `per second: ${perSec()}`
	perClickCount.innerHTML = `per click: ${creditsPerClick()}`

	//ami most jön 3, nem kéne minden képkockán újra renderelni, jobb lenne eseményvezérelten, de lusta vagyok HE-HE
	//render buildings
	var newBuildingElem = document.createElement("div")

	for (let building of buildings) {

		let elem = createBuildingElem(building)
		newBuildingElem.appendChild(elem)

	}
	buildingsDiv.innerHTML = ''
	buildingsDiv.appendChild(newBuildingElem)

	//render not unlocked upgrades
	var newUpgradeElem = document.createElement("div")
	for (let currU of upgrades) {

		if (!state.upgradesUnlocked[currU.id] && currU.condition()) {
			let elem = createUpgradeElem(currU)
			newUpgradeElem.appendChild(elem)
		}
	}
	upgradesDiv.innerHTML = ''
	upgradesDiv.appendChild(newUpgradeElem)

	//render unlocked upgrades 
	var newUnlUpgradeElem = document.createElement("div")
	for (let currU of upgrades) {
		if (state.upgradesUnlocked[currU.id]) {
			let elem = createUpgradeElem(currU, true)
			newUnlUpgradeElem.appendChild(elem)
		}
	}
	unlockedUpgrades.innerHTML = ''
	unlockedUpgrades.appendChild(newUnlUpgradeElem)
}

const earnRelUpgrades = [11, 12, 13, 14]

function earn(money) {
	state.credits += money
}

//init
var loadedState = localStorage.getItem("state")
if (loadedState) {
	state = JSON.parse(loadedState)
}
/**
 * klikk eseménykezelő
 */
creditBtn.addEventListener("mousedown", () => {
	earn(creditsPerClick())
})

var fps = 20
/**
 * jöjjön a della folyton
 */
var interval = setInterval(() => {
	earn(perSec())
	renderState()
}, 1000 / fps)
/**
 * játék mentés 60 secenként
 */
var saveInterval = setInterval(() => {
	save()
}, 60 * 1000)

renderState()